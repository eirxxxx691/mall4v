export const tableOption = {
  searchMenuSpan: 6,
  columnBtn: false,
  border: true,
  // selection: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  menuWidth: 350,
  align: 'center',
  refreshBtn: true,
  searchSize: 'mini',
  addBtn: false,
  editBtn: false,
  delBtn: false,
  viewBtn: false,
  props: {
    label: 'label',
    value: 'value'
  },
  column: [{
    label: '序号',
    prop: 'id',
    search: true
  }, {
    label: '会员名称',
    prop: 'account',
    search: true
  }, {
    label: '文件名称',
    prop: 'name',
    search: true
  }, {
    label: '路径',
    prop: 'path',
    slot: true
  }, {
    label: '时间',
    prop: 'createTime',
    slot: true
  }]
}
