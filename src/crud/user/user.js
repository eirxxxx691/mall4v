function mapRank (level) {
  switch (level) {
    case 1:
      return '青铜'
    case 2:
      return '白银'
    case 3:
      return '黄金'
    case 4:
      return '白金'
    case 5:
      return '钻石'
    case 6:
      return '黑钻'
    case 7:
      return '星耀'
    default:
      return '未定级'
  }
}

export const tableOption = {
  searchMenuSpan: 6,
  columnBtn: false,
  border: true,
  // selection: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  menuWidth: 350,
  align: 'center',
  refreshBtn: true,
  searchSize: 'mini',
  addBtn: false,
  editBtn: false,
  delBtn: false,
  viewBtn: false,
  props: {
    label: 'label',
    value: 'value'
  },
  column: [{
    label: '用户昵称',
    prop: 'nickName',
    search: true
  }, {
    label: '会员等级',
    prop: 'grade',
    slot: true,
    formatter: function (row, column, cellValue, index) {
      // 使用传统的函数声明
      return mapRank(cellValue)
    }
  }, {
    label: '状态',
    prop: 'status',
    search: true,
    type: 'select',
    slot: true,
    dicData: [
      {
        label: '禁用',
        value: 0
      }, {
        label: '正常',
        value: 1
      }
    ]
  }, {
    label: '注册时间',
    prop: 'userRegtime',
    imgWidth: 150
  }]
}
