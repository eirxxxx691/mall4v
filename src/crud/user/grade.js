export const tableOption = {
  searchMenuSpan: 6,
  columnBtn: false,
  border: true,
  // selection: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  menuWidth: 350,
  align: 'center',
  refreshBtn: true,
  searchSize: 'mini',
  addBtn: false,
  editBtn: false,
  delBtn: false,
  viewBtn: false,
  props: {
    label: 'label',
    value: 'value'
  },
  column: [{
    label: '会员序号',
    prop: 'id',
    search: true
  }, {
    label: '会员等级',
    prop: 'name',
    search: true
  }, {
    label: '会员折扣',
    prop: 'discount',
    slot: true
  }, {
    label: '邀请码',
    prop: 'invitation',
    slot: true
  }]
}
